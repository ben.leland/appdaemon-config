import appdaemon.plugins.hass.hassapi as hass
import datetime

# Living room light automation
# Each day time on and time off events are calculated and scheduled.
# Cloud cover offset is applied to sunrise/sunset time to account for light changes
#
# Args:
#   

class LivingRoom(hass.Hass):

  def initialize(self):

    runtime_morning = datetime.time(6, 0, 0)
    self.handle_morning = self.run_daily(self.morning, runtime_morning)
    # self.morning(self) # testing
    
    runtime_evening = datetime.time(13, 30, 0)
    self.handle_evening = self.run_daily(self.evening, runtime_evening)
    # self.evening(self) # testing

  def morning(self, kwargs):

    self.input_boolean = "input_boolean.automation_living_room_morning"
    self.automation_enabled_value = self.get_state(self.input_boolean) 

    if self.automation_enabled_value == "off":
      self.log("Automation disabled, no morning lights set")
      return

    sunrise_state = self.get_state("sun.sun", attribute = "next_rising")
    sunrise = datetime.datetime.strptime(sunrise_state, "%Y-%m-%dT%H:%M:%S+00:00")
    self.log("LOG AM Sunrise: {}".format(sunrise))

    cloud_offset = self.get_app("cloud_offset").cloud_offset
    self.log("LOG AM Cloud offset: {} mins".format(cloud_offset))    

    morning_on_time = datetime.time(6, 30, 0)
    self.log("LOG AM Time on: {}".format(morning_on_time))

    morning_off_time = sunrise + datetime.timedelta(0, cloud_offset * 60)
    morning_off_time = morning_off_time.time()
    self.log("LOG AM Time off: {}".format(morning_off_time))


    if morning_off_time < morning_on_time:
      self.log("LOG AM: Off time is before on time, no events scheduled")
      return

    self.run_once(self.morning_on, morning_on_time)
    self.run_once(self.morning_off, morning_off_time)

  def morning_on(self, kwargs):
  
    self.call_service("scene/turn_on", entity_id = "scene.living_room_morning", transition = 2)
    self.log("LOG AM lights on")

  def morning_off(self, kwargs):

    self.call_service("light/turn_off", entity_id = "light.living_room")
    self.log("LOG AM lights off")
 

  def evening(self, kwargs):

    sunset_state = self.get_state("sun.sun", attribute = "next_setting")
    sunset = datetime.datetime.strptime(sunset_state, "%Y-%m-%dT%H:%M:%S+00:00")
    self.log("LOG PM Sunset: {}".format(sunset))

    cloud_offset = self.get_app("cloud_offset").cloud_offset
    self.log("LOG PM Cloud offset: {} mins".format(cloud_offset))    

    evening_on_time = sunset - datetime.timedelta(0, cloud_offset * 60)
    evening_on_time = evening_on_time.time()
    self.log("LOG PM Time on: {}".format(evening_on_time))

    evening_dim_time = datetime.time(19, 30, 0)
    self.log("LOG PM Time dim: {}".format(evening_dim_time))

    evening_off_time = datetime.time(0, 0, 0)
    self.log("LOG PM Time off: {}".format(evening_off_time))

    if evening_on_time < evening_dim_time:
      self.run_once(self.evening_on, evening_on_time)
    
    self.run_once(self.evening_dim, evening_dim_time)
    self.run_once(self.evening_off, evening_off_time)

  def evening_on(self, kwargs):
  
    self.call_service("scene/turn_on", entity_id = "scene.living_room_afternoon", transition = 2)
    self.log("LOG PM lights on")

  def evening_dim(self, kwargs):
    
    self.call_service("scene/turn_on", entity_id = "scene.living_room_evening", transition = 60)
    self.log("LOG PM lights dim")

  def evening_off(self, kwargs):

    self.call_service("light/turn_off", entity_id = "light.living_room")
    self.log("LOG PM lights off")