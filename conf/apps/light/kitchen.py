import appdaemon.plugins.hass.hassapi as hass
import datetime


# Kitchen light automation
# Fixed times for strip lights
#
# Args:
#   

class Kitchen(hass.Hass):

  def initialize(self):

    runtime_morning = datetime.time(6, 0, 0)
    self.handle_morning = self.run_daily(self.morning, runtime_morning)
    # self.morning(self) # testing
    
    runtime_evening = datetime.time(13, 30, 0)
    self.handle_evening = self.run_daily(self.evening, runtime_evening)
    # self.evening(self) # testing

  def morning(self, kwargs):

    morning_on_time = datetime.time(6, 30, 0)
    self.log("LOG AM Time on: {}".format(morning_on_time))

    morning_off_time = datetime.time(9, 0, 0)
    self.log("LOG AM Time off: {}".format(morning_off_time))

    self.run_once(self.morning_on, morning_on_time)
    self.run_once(self.morning_off, morning_off_time)

  def morning_on(self, kwargs):
  
    self.call_service("switch/turn_on", entity_id = "switch.kitchen_cabinets")

  def morning_off(self, kwargs):

    self.call_service("switch/turn_off", entity_id = "switch.kitchen_cabinets")
 

  def evening(self, kwargs):

    evening_on_time = datetime.time(18, 0, 0)
    self.log("LOG PM Time on: {}".format(evening_on_time))

    evening_off_time = datetime.time(0, 0, 0)
    self.log("LOG PM Time off: {}".format(evening_off_time))
    
    self.run_once(self.evening_on, evening_on_time)
    self.run_once(self.evening_off, evening_off_time)

  def evening_on(self, kwargs):
  
    self.call_service("switch/turn_on", entity_id = "switch.kitchen_cabinets")

  def evening_off(self, kwargs):

    self.call_service("switch/turn_off", entity_id = "switch.kitchen_cabinets")