import appdaemon.plugins.hass.hassapi as hass
import datetime

# Triggers morning sunrise on hue lights in bedroom based on phone alarm times.
# Tasker sends next alarm via MQTT.
# HA listens to MQTT sensor and triggers hue lights for a calculated time before the alarm.
# Clears MQTT sensor in Homeassistant once it has done
# Schedules turn off event for 1 hour after alarm time
#
# Requires the following components to be available in Homeassistant:
#   - sensor.mqtt_phone_alarm_<name>
#   - light.<name>
#   - input_boolean.automations_alarm_<name>
# 
# Args:
#   wakee : name of person

class AlarmRise(hass.Hass):

  def initialize(self):

    self.mqtt_topic = "phone/alarm/{}".format(self.args["wakee"])
    self.sensor = "sensor.mqtt_phone_alarm_{}".format(self.args["wakee"])
    self.light_name = "light.{}".format(self.args["wakee"])
    self.input_boolean = "input_boolean.automations_alarm_{}".format(self.args["wakee"])
    # self.call_service("mqtt/publish", topic = self.mqtt_topic, payload = "")

    self.handle = self.listen_state(self.alarm_set, entity = self.sensor)
    # self.alarm_set(self) # for debugging


  def alarm_set(self, entity, attribute, old_state, new_state, kwargs):
    
    self.automation_enabled_value = self.get_state(self.input_boolean) 

    if self.automation_enabled_value == "off":
      self.log("Automation disabled, no alarm set")
      return

    self.sensor_value = self.get_state(entity)

    try:
      self.alarm_time = datetime.datetime.strptime(self.sensor_value, "%Y-%m-%d %H:%M")    
    except:
      self.error("Message did not convert to datetime {}".format(self.sensor_value), level = "WARNING")
      self.call_service("mqtt/publish", topic = self.mqtt_topic, payload = "")
      return
    
    self.log("Alarm detected at {}".format(self.alarm_time.strftime("%Y-%m-%d %H:%M")))

    # only included for debugging, function should only trigger when sensor is given a value
    if self.sensor_value == "":
      self.error("No alarm reported for {}".format(self.args["wakee"]), level = "WARNING")
      return

    # as Tasker submits the next alarm each day at 3am, action will be taken the following day instead
    if self.alarm_time.strftime("%Y-%m-%d") != datetime.datetime.now().strftime("%Y-%m-%d"):
      self.call_service("mqtt/publish", topic = self.mqtt_topic, payload = "")
      self.log("Alarm is not set for current day")
      return

    self.light_time_1 = self.alarm_time - datetime.timedelta(minutes = 20)
    self.light_time_2 = self.alarm_time - datetime.timedelta(minutes = 10)
    self.light_time_3 = self.alarm_time + datetime.timedelta(minutes = 60)
   
    self.run_once(self.transition_1, start = self.light_time_1.time())
    self.run_once(self.transition_2, start = self.light_time_2.time())
    self.run_once(self.light_off, start = self.light_time_3.time())

    self.log("Sunrise set for {}".format(self.light_time_1.strftime("%Y-%m-%d %H:%M")))
    # self.call_service("mqtt/publish", topic = self.mqtt_topic, payload = "")


  def transition_1(self, kwargs):

    self.call_service("light/turn_on", entity_id = self.light_name, kelvin = "2200", brightness_pct = "30", transition = "599")


  def transition_2(self, kwargs):

    self.call_service("light/turn_on", entity_id = self.light_name, kelvin = "5600", brightness_pct = "100", transition = "600")


  def light_off(self, kwargs):

    self.call_service("light/turn_off", entity_id = self.light_name)