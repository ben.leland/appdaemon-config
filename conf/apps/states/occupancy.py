import appdaemon.plugins.hass.hassapi as hass
from fsm import Fsm, State, Eq, Neq, LT, LE, GT, GE, Transition, Condition

class Occupancy(hass.Hass):

  def initialize(self):

    Occupancy = Fsm(self, id = 'occupancy', entity = 'input_select.state_occupancy', states = [
      State(id = 'HOME', transitions = [
        Transition(next = 'LEFT', conditions = [
          Condition(entity = 'group.residents', operand = 'not_home')
          # Condition(entity = 'input_boolean.state_occupancy_testing_presence', operand = 'off')
        ])
      ]),
    
      State(id = 'LEFT', transitions = [
        Transition(next = 'HOME', conditions = [
          Condition(entity = 'group.residents', operand = 'home')
          # Condition(entity = 'input_boolean.state_occupancy_testing_presence', operand = 'on')
        ]),
        Transition(next = 'OUT', conditions = [
          Condition(timeout_time = 10*60)
        ])        
      ]),
    
      State(id = 'OUT', transitions = [
        Transition(next = 'ARRIVED', conditions = [
          Condition(entity = 'group.residents', operand = 'home')
          # Condition(entity = 'input_boolean.state_occupancy_testing_presence', operand = 'on')
        ]),
        Transition(next = 'AWAY', conditions = [
          Condition(timeout_time = 18*60*60)          
        ]),
        Transition(next = 'AWAY', conditions = [
          Condition(entity = 'input_boolean.state_occupancy_away', operand = 'on'),
        ]),
        Transition(next = 'HOLIDAY', conditions = [
          Condition(entity = 'input_boolean.state_occupancy_holiday', operand = 'on'),
        ])        
      ]),
    
      State(id = 'AWAY', exit_programs = [ self.disable_overrides ], transitions = [
        Transition(next = 'ARRIVED', conditions = [
          Condition(entity = 'group.residents', operand = 'home')
          # Condition(entity = 'input_boolean.state_occupancy_testing_presence', operand = 'on')
        ]),
        Transition(next = 'HOLIDAY', conditions = [
          Condition(entity = 'input_boolean.state_occupancy_holiday', operand = 'on'),
          Condition(timeout_time = 15)
        ]),
        Transition(next = 'HOLIDAY', conditions = [
          Condition(timeout_time = 4*24*60*60)
        ])
      ]),

      State(id = 'HOLIDAY', exit_programs = [ self.disable_overrides ], transitions = [
        Transition(next = 'ARRIVED', conditions = [
          Condition(entity = 'group.residents', operand = 'home')
          # Condition(entity = 'input_boolean.state_occupancy_testing_presence', operand = 'on')
        ]),
        Transition(next = 'AWAY', conditions = [
          Condition(entity = 'input_boolean.state_occupancy_away', operand = 'on'),
          Condition(timeout_time = 15)
        ]),
      ]),

      State(id = 'ARRIVED', transitions = [
        Transition(next = 'HOME', conditions = [
          Condition(timeout_time = 15*60)
        ]),
        Transition(next = 'LEFT', conditions = [
          Condition(entity = 'group.residents', operand = 'away')
          # Condition(entity = 'input_boolean.state_occupancy_testing_presence', operand = 'off')
        ])
      ])

    ])

    Occupancy.log_graph_link()

  class disable_overrides:
    
    def program(self):

      self.hass.call_service('input_boolean/turn_off', entity_id = 'input_boolean.state_occupancy_away')
      self.hass.call_service('input_boolean/turn_off', entity_id = 'input_boolean.state_occupancy_holiday')