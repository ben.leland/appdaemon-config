import appdaemon.plugins.hass.hassapi as hass
from fsm import Fsm, State, Eq, Neq, LT, LE, GT, GE, Transition, Condition

class HeatingStudy(hass.Hass):

  def initialize(self):

    Occupancy = Fsm(self, id = 'heating_study', entity = 'input_select.state_heating', states = [
      State(id = 'OFF', transitions = [
        Transition(next = 'SCHEDULE', conditions = [
          Condition(entity = 'input_boolean.heating_study', operand = 'on')
        ])
      ]),
      State(id = 'SCHEDULE', transitions = [
        Transition(next = 'OVERRIDE', conditions = [
          Condition(entity = 'group.heating_override', operand = 'on')
        ])
      ]),
      State(id = 'OVERRIDE', transitions = [
        Transition(next = 'OFF', conditions = [
          Condition(entity = 'input_boolean.heating_study', operand = 'off')
        ]),
        Transition(next = 'SCHEDULE', conditions = [
          Condition(entity = 'group.heating_override', operand = 'off')
        ]),
      ])
    ])

class HeatingLivingRoom(hass.Hass):

  def initialize(self):

    Occupancy = Fsm(self, id = 'heating_study', entity = 'input_select.state_heating', states = [
      State(id = 'OFF', transitions = [
        Transition(next = 'SCHEDULE', conditions = [
          Condition(entity = 'group.residents', operand = 'away')
        ])
      ]),
      State(id = 'SCHEDULE', transitions = [
        Transition(next = 'OVERRIDE', conditions = [
          Condition(entity = 'group.residents', operand = 'away')
        ])
      ]),
      State(id = 'OVERRIDE', transitions = [
        Transition(next = 'SCHEDULE', conditions = [
          Condition(entity = 'group.residents', operand = 'away')
        ])
      ])
    ])