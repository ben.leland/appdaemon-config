import appdaemon.plugins.hass.hassapi as hass
from fsm import Fsm, State, Eq, Neq, LT, LE, GT, GE, Transition, Condition

class Visitors(hass.Hass):

  def initialize(self):

    Visitors = Fsm(self, id = 'hosting', entity = 'input_select.state_hosting', states = [
      State(id = 'NONE', transitions = [
        Transition(next = 'VISITORS', conditions = [
          # Condition(entity = 'binary_sensor.wifi_guests', operand = 'on')
          Condition(entity = 'input_boolean.state_hosting_testing_wifi', operand = 'on'),
          Condition(timeout_time = 30)
        ]),
        Transition(next = 'VISITORS', conditions = [
          Condition(entity = 'input_boolean.state_hosting_visitors', operand = 'on')
        ]),
        Transition(next = 'GUESTS', programs = [ self.VisitorsBooleanOn ], conditions = [
          Condition(entity = 'group.state_hosting_guest_rooms', operand = 'on')
        ]),
      ]),
      State(id = 'VISITORS', transitions = [
        Transition(next = 'GUESTS', conditions = [
          Condition(entity = 'group.state_hosting_guest_rooms', operand = 'on')
        ]),
        Transition(next = 'NONE', conditions = [
          # Condition(entity = 'binary_sensor.wifi_guests', operand = 'off')
          Condition(entity = 'input_boolean.state_hosting_testing_wifi', operand = 'off'),
          Condition(entity = 'input_boolean.state_hosting_visitors', operand = 'off')
        ])
      ]),
      State(id = 'GUESTS', transitions = [
        Transition(next = 'NONE', conditions = [
          Condition(entity = 'group.state_hosting_guest_rooms', operand = 'off')
        ])
      ])            
    ])

  class VisitorsBooleanOn:
    
    def program(self):

      self.hass.call_service('input_boolean/turn_on', entity_id = 'input_boolean.state_hosting_visitors')

# class GuestRooms(hass.Hass):

#   def initialize(self):

#     self.room_boolean  = "input_boolean.state_hosting_guest_{}".format(self.args['room'])

#     GuestRooms = Fsm(self, id = 'guest_rooms', entity = 'input_select.occupancy_state', states = [
#       State(id = 'HOME', transitions = [
#         Transition(next = 'LEFT', conditions = [
#           Condition(entity = 'group.residents', operand = 'away')
#         ])
#       ]),
#     ])