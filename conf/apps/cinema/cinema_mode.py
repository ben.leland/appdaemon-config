import appdaemon.plugins.hass.hassapi as hass
import datetime
import time
import pafy
# import youtube_dl # needed for pafy
import random


# Living room light automation
# Each day time on and time off events are calculated and scheduled.
# Cloud cover offset is applied to sunrise/sunset time to account for light changes
#
# Args:
#

class CinemaMode(hass.Hass):

    def initialize(self):
        self.listen_state(self.activate, entity="input_boolean.cinema_mode", new="on")
        self.listen_state(self.deactivate, entity="input_boolean.cinema_mode", new="off")
        self.volume_boost = 21
        self.num_trailers = 2
        self.running = False

        self.thx_url = "https://www.youtube.com/watch?v=86IrVpIY3tY"
        self.vue_url = "https://www.youtube.com/watch?v=FzyAjoBLzY8"

    def activate(self, entity, attribute, old_state, new_state, kwargs):
        self.running = True

        # increase volume
        self.call_service("mqtt/publish",
                          topic="house/media/stereo",
                          payload="".join(['u'] * self.volume_boost))

        light_names = ["light.hanging", "light.lamp", "light.bookcase"]
        for light_name in light_names:
            self.call_service("light/turn_on", entity_id=light_name,
                              color_temp=2500,
                              brightness=30,
                              transition=5,
                              )

        wait_time = getDuration(self.vue_url) - 13 # cuts off vue logo
        self.play_youtube(t_url=self.vue_url)
        trailer_purl = "https://www.youtube.com/watch?v=ee1172yeqyE&list=PLScC8g4bqD47c-qHlsfhGH3j6Bg7jzFy-"
        playlist = pafy.get_playlist(trailer_purl)

        for _ in range(self.num_trailers):
            vid = random.choice(playlist['items'])
            t_url = vid['pafy'].getbest().url
            self.run_in(self.play_youtube, wait_time, t_url=t_url)
            wait_time += getDuration(t_url) - 6

        self.run_in(self.play_youtube(self.thx_url), wait_time)
        wait_time += getDuration(self.thx_url)
        self.run_in(self.end_activate, wait_time)

    def play_youtube(self, t_url=None):
        self.call_service("media_extractor/play_media",
                          entity_id="media_player.chromecastultra",
                          media_content_type="video/youtube",
                          media_content_id=t_url)

    def end_activate(self):
        self.running = False

    def deactivate(self):
        # decrease volume
        self.call_service("mqtt/publish",
                          topic="house/media/stereo",
                          payload="".join(['d'] * self.volume_boost))
        # this could also be a single call to 'r' but that would cause the volume to go through 0 (maybe good maybe bad)


def getDuration(url):
    video = pafy.new(url)
    return video.length
